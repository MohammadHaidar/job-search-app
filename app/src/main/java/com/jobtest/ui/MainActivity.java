package com.jobtest.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jobtest.adapters.JobsAdapter;
import com.jobtest.interfaces.GetJobDetailsAPI;
import com.jobtest.interfaces.GetJobsAPI;
import com.jobtest.interfaces.OnLoadMoreListener;
import com.jobtest.models.POJO.JobDetails;
import com.jobtest.models.POJO.JobPost;
import com.jobtest.models.POJO.Jobs;
import com.jobtest.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.navigation) BottomNavigationView navigation;
    @BindView(R.id.progressBarHolder)  FrameLayout progressBarHolder;
    @BindView(R.id.recycleView)  RecyclerView mRecyclerView;
    @BindView(R.id.search_input_edit_text) public EditText searchInputEditText;

    private List<JobPost> jobsList = new ArrayList<>();
    private List<String> keywordsList = new ArrayList<>();
    private JobsAdapter mJobsAdapter;
    private int pageNumber = 1;
    private int jobCount = 0;
    private AlphaAnimation inAnimation;
    private AlphaAnimation outAnimation;
    private JobDetailsDialog jobDetailsDialog;
    private String GET_JOBS_BASE_URL = "https://api.bayt.com/m/jobs";
    private String GET_JOB_DETAILS_BASE_URL = "https://api.bayt.com/m/job";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //call search api when user press search on the keyboard
        searchInputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getInitialResults(v.getText().toString());
                    return true;
                }
                return false;
            }
        });

        //initiate recycle view
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //For demo purpose
        getInitialResults("Product Manager");
        searchInputEditText.setText("Product Manager");

        //initialize animations to be used with the dialog
        inAnimation = new AlphaAnimation(0f, 1f);
        inAnimation.setDuration(200);
        outAnimation = new AlphaAnimation(1f, 0f);
        outAnimation.setDuration(200);

    }

    //show the loader when calling api
    public void showLoader() {
        progressBarHolder.setAnimation(inAnimation);
        progressBarHolder.setVisibility(View.VISIBLE);
    }

    //hide the loader when api call is finished
    public void hideLoader() {
        progressBarHolder.setAnimation(outAnimation);
        progressBarHolder.setVisibility(View.GONE);
    }



    public void jobClicked(final JobPost job, final int position) {

        showLoader();
        String getJobDetailsAPIBaseURL = GET_JOB_DETAILS_BASE_URL;

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(getJobDetailsAPIBaseURL).build();

        //Creating Rest Services
        final GetJobDetailsAPI getJobDetailsAPI = adapter.create(GetJobDetailsAPI.class);

        getJobDetailsAPI.getJobDetails(job.getJobID(), new Callback<JobDetails>() {
            @Override
            public void success(JobDetails jobDetails, retrofit.client.Response response) {

                //dismiss the dialog if it is already displayed
                if (jobDetailsDialog != null)
                    if (jobDetailsDialog.isShowing())
                        jobDetailsDialog.dismiss();

                //set the job as viewed so that the title will not appear in bold anymore
                for (int i = 0; i < jobsList.size(); ++i) {
                    if (jobsList.get(i).getJobID().equals(jobDetails.getJobs().get(0).get(0).getJobID()))
                        jobsList.get(i).setViewed(true);
                }


                //the dialog that opens showing the clicked dialog
                jobDetailsDialog = new JobDetailsDialog(MainActivity.this, jobDetails.getJobs().get(0).get(0), position) {

                    @Override
                    public void nextButtonClicked(int position) {
                        jobDetailsDialog.cancel();
                        if (position < jobsList.size() - 1)
                            jobClicked(jobsList.get(position + 1), position + 1);

                    }

                    @Override
                    public void backButtonClicked(int position) {
                        jobDetailsDialog.cancel();
                        if (position > 0)
                            jobClicked(jobsList.get(position - 1), position - 1);
                    }
                };

                //notify the adapter that data had been changed to redraw changes
                jobDetailsDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        mJobsAdapter.notifyDataSetChanged();
                    }
                });
                jobDetailsDialog.show();
                hideLoader();
            }

            @Override
            public void failure(RetrofitError error) {
                if (jobDetailsDialog != null)
                    jobDetailsDialog.show();
                hideLoader();
            }
        });

    }

    //get the first results when app is launched
    public void getInitialResults(final String searchField) {
        String getJobsAPIBaseURL = GET_JOBS_BASE_URL;
        showLoader();
        mRecyclerView.setAdapter(null);
        keywordsList.clear();
        jobsList.clear();
        pageNumber = 1;
        jobCount = 0;


        //making object of RestAdapter
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(getJobsAPIBaseURL).build();

        //Creating Rest Services
        final GetJobsAPI getJobsAPI = adapter.create(GetJobsAPI.class);

        getJobsAPI.getJobs(searchField, "ae", "10", pageNumber + "", new Callback<Jobs>() {
            @Override
            public void success(Jobs jobs, retrofit.client.Response response) {

                jobsList.addAll(jobs.getJobPosts());
                keywordsList = jobs.getSuggestedKeywords();

                mJobsAdapter = new JobsAdapter(MainActivity.this, keywordsList, jobsList, mRecyclerView, searchField);
                mRecyclerView.setAdapter(mJobsAdapter);
                //set the functionality to load more jobs when list end is reached
                mJobsAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {

                        jobsList.add(null);
                        mJobsAdapter.notifyItemInserted(jobsList.size() - 1);
                        //Load more data for reyclerview
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {

                                getJobsAPI.getJobs(searchField, "ae", "10", pageNumber + "", new Callback<Jobs>() {
                                    @Override
                                    public void success(Jobs jobs, retrofit.client.Response response) {
                                        jobsList.remove(jobsList.size() - 1);
                                        mJobsAdapter.notifyItemRemoved(jobsList.size());
                                        jobsList.addAll(jobs.getJobPosts());
                                        mJobsAdapter.notifyDataSetChanged();
                                        jobCount += jobs.getJobPosts().size();
                                        if (jobCount >= jobs.getSearchCount()) {
                                            pageNumber++;
                                            mJobsAdapter.setReachedMax(true);
                                        } else
                                            mJobsAdapter.setLoaded();

                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        mJobsAdapter.setLoaded();
                                        jobsList.remove(jobsList.size() - 1);
                                        mJobsAdapter.notifyItemRemoved(jobsList.size());
                                    }
                                });
                            }
                        });
                    }
                });


                mJobsAdapter.notifyDataSetChanged();
                jobCount = jobs.getJobPosts().size();
                if (jobCount >= jobs.getSearchCount()) {
                    pageNumber++;
                    mJobsAdapter.setReachedMax(true);
                }

                hideLoader();

            }

            @Override
            public void failure(RetrofitError error) {
                hideLoader();
            }
        });
    }


}
