package com.jobtest.ui;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jobtest.models.POJO.Job;
import com.jobtest.R;
import com.jobtest.helpers.Utils;
import com.squareup.picasso.Picasso;

/**
 * Created by Mohammad Haidar on 9/16/2017.
 */

public abstract class JobDetailsDialog extends Dialog
{


    private RelativeLayout containerRL;
    private ImageView companyLogoImageView;
    private ImageView favoriteImageView;
    private TextView jobTitleTextView;
    private TextView companyNameTextView;
    private TextView regionTextView;
    private TextView dateTextView;
    private TextView jobDescriptionValueTextView;
    private TextView skillsValueTextView;
    private ImageView backButton;
    private ImageView nextButton;
    private ImageView closeButton;
    private Activity activity;
    private Job job;
    private int position;



    public JobDetailsDialog(Activity activity, Job job, int position) {
        super(activity);
        this.activity = activity;
        this.job = job;
        this.position = position;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_job_details);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

        companyLogoImageView = findViewById(R.id.company_logo_image_view);
        favoriteImageView = findViewById(R.id.favorite_image_view);
        jobTitleTextView = findViewById(R.id.job_title_text_view);
        companyNameTextView = findViewById(R.id.company_name_text_view);
        regionTextView = findViewById(R.id.regoin_text_view);
        dateTextView = findViewById(R.id.date_text_view);
        containerRL = findViewById(R.id.container);
        jobDescriptionValueTextView = findViewById(R.id.job_description_value_text_view);
        skillsValueTextView = findViewById(R.id.skills_value_text_view);

        nextButton = findViewById(R.id.next_button);
        backButton = findViewById(R.id.back_button);
        closeButton = findViewById(R.id.close_button);


        jobDescriptionValueTextView.setText(job.getJobDescription());
        skillsValueTextView.setText(job.getJobSkills());


        jobTitleTextView.setText(job.getJobTitle());
        companyNameTextView.setText(job.getCompanyName());
        regionTextView.setText(job.getRegion());
        if(Utils.getDate(job.getPostedOn()).equals("Today"))
            dateTextView.setTextColor(activity.getResources().getColor(R.color.search_layout_color));



        dateTextView.setText(Utils.getDate(job.getPostedOn()));
        if(!job.getCompanyLogo().isEmpty())
            Picasso.with(activity).load(job.getCompanyLogo()).into(companyLogoImageView);


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextButtonClicked(position);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backButtonClicked(position);
            }
        });


    }

    public abstract void nextButtonClicked(int position);
    public abstract void backButtonClicked(int position);

}
