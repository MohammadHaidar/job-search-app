package com.jobtest.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Mohammad Haidar on 9/16/2017.
 */

public class Utils
{

    public static String getDate(String date)
    {
        SimpleDateFormat receivedDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat resultDateFormat = new SimpleDateFormat("MMM dd", Locale.getDefault());

        Date convertedDate = new Date();
        try {
            convertedDate = receivedDateFormat.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        long dateMillis = convertedDate.getTime();
        long nowMillis = System.currentTimeMillis();
        long diff = nowMillis - dateMillis;


        double days = (double) (diff / (1000*60*60*24));

        if(days<1)
            return "Today";
        else if(days<7) {
            try {
                return getWeekDay(receivedDateFormat.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
            return resultDateFormat.format(convertedDate);

        return "";

    }

    public static String getWeekDay(Date date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        return sdf.format(date).substring(0,3);
    }

}
