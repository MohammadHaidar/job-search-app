package com.jobtest.models;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jobtest.helpers.Utils;
import com.jobtest.models.POJO.JobPost;
import com.jobtest.R;
import com.jobtest.ui.MainActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by Mohammad Haidar on 9/20/2017.
 */

public class JobViewHolder extends BaseViewHolder<JobPost>
{

    private RelativeLayout containerRL;
    private ImageView companyLogoImageView;
    private ImageView favoriteImageView;
    private TextView jobTitleTextView;
    private TextView companyNameTextView;
    private TextView regionTextView;
    private TextView dateTextView;
    private Activity activity;

    public JobViewHolder(ViewGroup parent,Activity activity)
    {
        super(parent,R.layout.layout_job_item);


        this.activity = activity;
        companyLogoImageView = itemView.findViewById(R.id.company_logo_image_view);
        favoriteImageView = itemView.findViewById(R.id.favorite_image_view);
        jobTitleTextView = itemView.findViewById(R.id.job_title_text_view);
        companyNameTextView = itemView.findViewById(R.id.company_name_text_view);
        regionTextView = itemView.findViewById(R.id.regoin_text_view);
        dateTextView = itemView.findViewById(R.id.date_text_view);
        containerRL = itemView.findViewById(R.id.container);

    }

    @Override
    public void setData(final JobPost item) {
         jobTitleTextView.setText(item.getJobTitle());
         companyNameTextView.setText(item.getCompanyName());
         regionTextView.setText(item.getRegion());
        if(Utils.getDate(item.getPostedOn()).equals("Today"))
             dateTextView.setTextColor(activity.getResources().getColor(R.color.search_layout_color));

        if(!item.isViewed())
        {
             dateTextView.setTypeface(Typeface.DEFAULT_BOLD);
             jobTitleTextView.setTypeface(Typeface.DEFAULT_BOLD);
        }
        else
        {
             dateTextView.setTypeface(Typeface.DEFAULT);
             jobTitleTextView.setTypeface(Typeface.DEFAULT);
        }


         dateTextView.setText(Utils.getDate(item.getPostedOn()));
        if(!item.getCompanyLogo().isEmpty())
            Picasso.with(activity).load(item.getCompanyLogo()).into( companyLogoImageView);


         containerRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity)activity).jobClicked(item,item.getPosition());
            }
        });
    }
}