
package com.jobtest.models.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobPost {

    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("companyLogo")
    @Expose
    private String companyLogo;
    @SerializedName("jobTitle")
    @Expose
    private String jobTitle;
    @SerializedName("jobID")
    @Expose
    private String jobID;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("postedOn")
    @Expose
    private String postedOn;
    @SerializedName("isAggregated")
    @Expose
    private String isAggregated;

    private boolean viewed;

    private int position;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }

    public String getIsAggregated() {
        return isAggregated;
    }

    public void setIsAggregated(String isAggregated) {
        this.isAggregated = isAggregated;
    }


    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
