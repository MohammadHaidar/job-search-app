
package com.jobtest.models.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Jobs {

    @SerializedName("searchCount")
    @Expose
    private Integer searchCount;
    @SerializedName("suggestedKeywords")
    @Expose
    private List<String> suggestedKeywords = null;
    @SerializedName("jobPosts")
    @Expose
    private List<JobPost> jobPosts = null;

    public Integer getSearchCount() {
        return searchCount;
    }

    public void setSearchCount(Integer searchCount) {
        this.searchCount = searchCount;
    }

    public List<String> getSuggestedKeywords() {
        return suggestedKeywords;
    }

    public void setSuggestedKeywords(List<String> suggestedKeywords) {
        this.suggestedKeywords = suggestedKeywords;
    }

    public List<JobPost> getJobPosts() {
        return jobPosts;
    }

    public void setJobPosts(List<JobPost> jobPosts) {
        this.jobPosts = jobPosts;
    }

}
