
package com.jobtest.models.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Job {

    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("companyLogo")
    @Expose
    private String companyLogo;
    @SerializedName("jobTitle")
    @Expose
    private String jobTitle;
    @SerializedName("jobDescription")
    @Expose
    private String jobDescription;
    @SerializedName("jobID")
    @Expose
    private String jobID;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("regionIso")
    @Expose
    private RegionIso regionIso;
    @SerializedName("alreadyApplied")
    @Expose
    private Boolean alreadyApplied;
    @SerializedName("webApplyOnly")
    @Expose
    private String webApplyOnly;
    @SerializedName("postedOn")
    @Expose
    private String postedOn;
    @SerializedName("isExternal")
    @Expose
    private String isExternal;
    @SerializedName("ExternalUrl")
    @Expose
    private String externalUrl;
    @SerializedName("questionnaireStatus")
    @Expose
    private String questionnaireStatus;
    @SerializedName("hasTranslation")
    @Expose
    private Boolean hasTranslation;
    @SerializedName("companyBadge")
    @Expose
    private Object companyBadge;
    @SerializedName("companyDescription")
    @Expose
    private String companyDescription;
    @SerializedName("jobSkills")
    @Expose
    private String jobSkills;
    @SerializedName("jobSpecialties")
    @Expose
    private List<JobSpecialty> jobSpecialties = null;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("jobDetails")
    @Expose
    private List<JobDetail> jobDetails = null;
    @SerializedName("preferredCandidate")
    @Expose
    private List<PreferredCandidate> preferredCandidate = null;
    @SerializedName("shareUrl")
    @Expose
    private String shareUrl;
    @SerializedName("job_active")
    @Expose
    private String jobActive;
    @SerializedName("jb_title_translated")
    @Expose
    private String jbTitleTranslated;
    @SerializedName("jb_desc_translated")
    @Expose
    private String jbDescTranslated;
    @SerializedName("jb_skills_translated")
    @Expose
    private String jbSkillsTranslated;
    @SerializedName("jb_education_translated")
    @Expose
    private String jbEducationTranslated;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public RegionIso getRegionIso() {
        return regionIso;
    }

    public void setRegionIso(RegionIso regionIso) {
        this.regionIso = regionIso;
    }

    public Boolean getAlreadyApplied() {
        return alreadyApplied;
    }

    public void setAlreadyApplied(Boolean alreadyApplied) {
        this.alreadyApplied = alreadyApplied;
    }

    public String getWebApplyOnly() {
        return webApplyOnly;
    }

    public void setWebApplyOnly(String webApplyOnly) {
        this.webApplyOnly = webApplyOnly;
    }

    public String getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }

    public String getIsExternal() {
        return isExternal;
    }

    public void setIsExternal(String isExternal) {
        this.isExternal = isExternal;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public String getQuestionnaireStatus() {
        return questionnaireStatus;
    }

    public void setQuestionnaireStatus(String questionnaireStatus) {
        this.questionnaireStatus = questionnaireStatus;
    }

    public Boolean getHasTranslation() {
        return hasTranslation;
    }

    public void setHasTranslation(Boolean hasTranslation) {
        this.hasTranslation = hasTranslation;
    }

    public Object getCompanyBadge() {
        return companyBadge;
    }

    public void setCompanyBadge(Object companyBadge) {
        this.companyBadge = companyBadge;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public String getJobSkills() {
        return jobSkills;
    }

    public void setJobSkills(String jobSkills) {
        this.jobSkills = jobSkills;
    }

    public List<JobSpecialty> getJobSpecialties() {
        return jobSpecialties;
    }

    public void setJobSpecialties(List<JobSpecialty> jobSpecialties) {
        this.jobSpecialties = jobSpecialties;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public List<JobDetail> getJobDetails() {
        return jobDetails;
    }

    public void setJobDetails(List<JobDetail> jobDetails) {
        this.jobDetails = jobDetails;
    }

    public List<PreferredCandidate> getPreferredCandidate() {
        return preferredCandidate;
    }

    public void setPreferredCandidate(List<PreferredCandidate> preferredCandidate) {
        this.preferredCandidate = preferredCandidate;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getJobActive() {
        return jobActive;
    }

    public void setJobActive(String jobActive) {
        this.jobActive = jobActive;
    }

    public String getJbTitleTranslated() {
        return jbTitleTranslated;
    }

    public void setJbTitleTranslated(String jbTitleTranslated) {
        this.jbTitleTranslated = jbTitleTranslated;
    }

    public String getJbDescTranslated() {
        return jbDescTranslated;
    }

    public void setJbDescTranslated(String jbDescTranslated) {
        this.jbDescTranslated = jbDescTranslated;
    }

    public String getJbSkillsTranslated() {
        return jbSkillsTranslated;
    }

    public void setJbSkillsTranslated(String jbSkillsTranslated) {
        this.jbSkillsTranslated = jbSkillsTranslated;
    }

    public String getJbEducationTranslated() {
        return jbEducationTranslated;
    }

    public void setJbEducationTranslated(String jbEducationTranslated) {
        this.jbEducationTranslated = jbEducationTranslated;
    }

}
