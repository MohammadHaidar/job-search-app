
package com.jobtest.models.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegionIso {

    @SerializedName("iso")
    @Expose
    private String iso;
    @SerializedName("prId")
    @Expose
    private String prId;
    @SerializedName("lcId")
    @Expose
    private String lcId;

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getPrId() {
        return prId;
    }

    public void setPrId(String prId) {
        this.prId = prId;
    }

    public String getLcId() {
        return lcId;
    }

    public void setLcId(String lcId) {
        this.lcId = lcId;
    }

}
