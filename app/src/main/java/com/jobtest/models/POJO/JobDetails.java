
package com.jobtest.models.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobDetails {

    @SerializedName("jobs")
    @Expose
    private List<List<Job>> jobs = null;

    public List<List<Job>> getJobs() {
        return jobs;
    }

    public void setJobs(List<List<Job>> jobs) {
        this.jobs = jobs;
    }

}
