package com.jobtest.models;

/**
 * Created by Mohammad Haidar on 9/20/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    BaseViewHolder(ViewGroup parent, int layoutId) {
        super(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
    }

    public abstract void setData(T item);
}