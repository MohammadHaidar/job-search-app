package com.jobtest.models;

import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.jobtest.R;

/**
 * Created by Mohammad Haidar on 9/20/2017.
 */

public class LoadingViewHolder extends BaseViewHolder<String>
{

    private ProgressBar progressBar;

    public LoadingViewHolder(ViewGroup parent)
    {
        super(parent,R.layout.layout_loading_item);
        progressBar = itemView.findViewById(R.id.progressBar1);
    }


    @Override
    public void setData(String item) {
        progressBar.setIndeterminate(true);
    }
}
