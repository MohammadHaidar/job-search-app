package com.jobtest.models;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jobtest.R;
import com.jobtest.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohammad Haidar on 9/20/2017.
 */

 public class SuggestedViewHolder extends BaseViewHolder<List<String>>
{

    private TextView relatedSearchTextView;
    private TextView firstKeywordTextView;
    private TextView secondKeywordTextView;
    private TextView thirdKeywordTextView;
    private TextView fourthKeywordTextView;
    private Activity activity;
    private String searchText;

    public SuggestedViewHolder(ViewGroup parent, Activity activity,String searchText)
    {
        super(parent,R.layout.layout_suggested_item);

        this.activity = activity;
        this.searchText = searchText;


        relatedSearchTextView = itemView.findViewById(R.id.related_search_text_view);
        firstKeywordTextView = itemView.findViewById(R.id.first_keyword_text_view);
        secondKeywordTextView = itemView.findViewById(R.id.second_keyword_text_view);
        thirdKeywordTextView = itemView.findViewById(R.id.third_keyword_text_view);
        fourthKeywordTextView = itemView.findViewById(R.id.fourth_keyword_text_view);
    }


    @Override
    public void setData(final List<String> item) {
        List<TextView> textViews = new ArrayList<>();
        textViews.add( firstKeywordTextView);
        textViews.add( secondKeywordTextView);
        textViews.add( thirdKeywordTextView);
        textViews.add( fourthKeywordTextView);


        for(int i = 0; i<item.size(); ++i)
        {
            textViews.get(i).setVisibility(View.VISIBLE);
            textViews.get(i).setText(item.get(i));
            final int finalI = i;
            textViews.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity)activity).searchInputEditText.setText(item.get(finalI));
                    ((MainActivity)activity).getInitialResults(item.get(finalI));

                }
            });
        }

         relatedSearchTextView.setText("\""+searchText+"\"");
    }
}
