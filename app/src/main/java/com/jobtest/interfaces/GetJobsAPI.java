package com.jobtest.interfaces;

import com.jobtest.models.POJO.Jobs;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Mohammad Haidar on 9/19/2017.
 */

public interface GetJobsAPI {

    @GET("/search")
    void getJobs(@Query("key") String key,@Query("region") String region,@Query("per_page") String per_page,@Query("page") String page, Callback<Jobs> cb);

}