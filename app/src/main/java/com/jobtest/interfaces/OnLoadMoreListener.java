package com.jobtest.interfaces;

/**
 * Created by Mohammad Haidar on 9/14/2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}