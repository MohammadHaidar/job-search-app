package com.jobtest.interfaces;

import com.jobtest.models.POJO.JobDetails;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Mohammad Haidar on 9/19/2017.
 */

public interface GetJobDetailsAPI {
    @GET("/view/{jobID}/")
    void getJobDetails(@Path("jobID") String jobID , Callback<JobDetails> cb);

}