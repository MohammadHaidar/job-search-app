package com.jobtest.adapters;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.jobtest.interfaces.OnLoadMoreListener;
import com.jobtest.models.JobViewHolder;
import com.jobtest.models.LoadingViewHolder;
import com.jobtest.models.POJO.JobPost;
import com.jobtest.models.SuggestedViewHolder;

import java.util.List;

/**
 * Created by Mohammad Haidar on 9/20/2017.
 */

public class JobsAdapter extends RecyclerView.Adapter < RecyclerView.ViewHolder >
{
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_SUGGESTED = 2;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean reachedMax=false;
    private List<JobPost> jobsList;
    private List<String> keywordsList;
    private Activity activity;
    private String searchText;
    private String s;

    public JobsAdapter(Activity activity,List<String> keywordsList, List<JobPost> jobsList, final RecyclerView mRecyclerView,String searchText)
    {
        this.jobsList = jobsList;
        this.keywordsList = keywordsList;
        this.activity = activity;
        this.searchText = searchText;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();

                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (mRecyclerView.getAdapter()!=null && !reachedMax && !isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold))
                {
                    if (mOnLoadMoreListener != null)
                    {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }


    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener)
    {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }



    @Override public int getItemViewType(int position)
    {
        return position == 4 && keywordsList.size()>0 ? VIEW_TYPE_SUGGESTED: jobsList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        if (viewType == VIEW_TYPE_ITEM)
            return new JobViewHolder(parent,activity);
         else if (viewType == VIEW_TYPE_LOADING)
            return new LoadingViewHolder(parent);
        else if(viewType == VIEW_TYPE_SUGGESTED)
            return new SuggestedViewHolder(parent,activity,searchText);



        return null;
    }
    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position)
    {

        if (holder instanceof JobViewHolder) {
            JobPost jobPost = jobsList.get(position);
            jobPost.setPosition(position);
            ((JobViewHolder) holder).setData(jobPost);


        } else if (holder instanceof LoadingViewHolder) {
            ((LoadingViewHolder) holder).setData(null);

        }
        else if (holder instanceof SuggestedViewHolder) {
            ((SuggestedViewHolder) holder).setData(keywordsList);
        }
    }

    @Override
    public int getItemCount() {
        return jobsList == null ? 0 : jobsList.size();
    }

    public void setLoaded()
    {
        isLoading = false;
    }

    public void setReachedMax(boolean reachedMax)
    {
        this.reachedMax = reachedMax;
    }

    public boolean isReachedMax() {
        return reachedMax;
    }
}